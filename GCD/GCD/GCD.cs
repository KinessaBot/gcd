﻿using System;
using System.Diagnostics;
using System.Threading;

namespace GCD
{
    class GCD
    {
        static public int x1 = 4518012;
        static public int x2 = 10237418;


        public static int GCD1(int a, int b)
        {

            return b == 0 ? a : GCD1(b, a % b);
        }

        public static void GCD1()
        {
            int a = 13;
            int b = 39;
            Console.WriteLine(b == 0 ? a : GCD1(b, a % b));
            Thread.Sleep(0);
        }

        public static int GCD2(int a, int b)
        {
            while (a != 0 && b != 0)
            {
                if (a > b)
                    a %= b;
                else
                    b %= a;
            }
            return a == 0 ? b : a;
        }

        public static void GCD2()
        {
            int a = 13;
            int b = 39;
            while (a != 0 && b != 0)
            {
                if (a > b)
                    a %= b;
                else
                    b %= a;
            }
            Console.WriteLine(a == 0 ? b : a);
            Thread.Sleep(0);
        }
    }
}
