﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Threading;


namespace GCD
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();
            Thread t1 = new Thread(new ThreadStart(GCD.GCD1));
            Thread t2 = new Thread(new ThreadStart(GCD.GCD2));
            stopwatch.Start();
            t1.Start();
            t1.Join();
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed);
            stopwatch.Restart();
            t2.Start();
            t2.Join();
            Console.WriteLine(stopwatch.Elapsed);
            Console.ReadLine();
        }
    }
}
